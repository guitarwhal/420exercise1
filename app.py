from flask import Flask, abort, flash, render_template, request
from BlogPostApp.post import Post, PostForm
from BlogPostApp.db import Database

app = Flask(__name__)
import secrets
app.secret_key = secrets.token_urlsafe(32)

db = Database()