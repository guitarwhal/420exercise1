import oracledb
from .post import Post
import os
class Database:
    def __init__(self, autocommit=True):
        self.__conn = oracledb.connect(user=os.environ['DBUSER'], password=os.environ['DBPWD'],
                                             host="198.168.52.211", port=1521, service_name="pdbora19c.dawsoncollege.qc.ca")
        self.__conn.autocommit = autocommit
    
    def run_file(self, file_path):
        statement_parts = []
        with self.__connection.cursor() as cursor:
            with open(file_path, 'r') as f:
                for line in f:
                    statement_parts.append(line)
                    if line.strip('\n').strip('\n\r').strip().endswith(';'):
                        statement = "".join(
                            statement_parts).strip().rstrip(';')
                        if statement:
                            try:
                                cursor.execute(statement)
                            except Exception as e:
                                print(e)
                        statement_parts = []
    
    def close(self):
        if self.__conn is not None:
            self.__conn.close()
            self.__conn = None
    
    def add_post(self, post):
        with self.__conn.cursor() as cursor:
            stmt = 'INSERT INTO flask_posts VALUES(:title,:id,:author,:tag_line,:text,:date)'
        try:
            cursor.execute(stmt,
                title=post.title, id=post.id, author=post.author, tag_line=post.tag_line, text=post.text, date=post.date)
        except Exception as e:
            print(e)
            
    def get_post(self, title):
        with self.__conn.cursor() as cursor:
            stmt = 'SELECT p_id, p_title, p_author, p_tag_line, p_text, p_date FROM flask_posts WHERE p_title=:title'
            try:
                result = cursor.execute(stmt,title=title)
                
                return Post(result[0],result[1],result[2],result[3],result[4], result[5])
            except Exception as e:
                print(e)
    
    def get_posts(self):
        with self.__conn.cursor() as cursor:
            stmt = 'SELECT p_id, p_title, p_author, p_tag_line, p_text, p_date FROM flask_posts'
            try:
                results = cursor.execute(stmt)
                posts = []
                
                for row in results:
                    post = Post(row[0],row[1],row[2],row[3],row[4],row[5])
                    posts.append(post)
                
                return posts
            except Exception as e:
                print(e)
              
              
    # Notes
      
    def add_comment(self, comment):
        with self.__conn.cursor() as cursor:
            stmt = 'INSERT INTO flask_comments VALUES(:id,:author,:text,:post_date,:post_id)'
        try:
            cursor.execute(stmt,
                id=comment.id, author=comment.author, text=comment.text, post_date=comment.post_date, post_id=comment.post_id)
        except Exception as e:
            print(e)