from flask import Blueprint, abort, flash, render_template, request
from BlogPostApp.db import Database
from BlogPostApp.post import Post, PostForm

bp = Blueprint('posts', __name__, url_prefix='/posts/')

def get_db():
    return Database()

@bp.route('/', methods=['GET','POST'])
def get_posts():
    try:
        posts = get_db().get_posts()
    except Exception as e:
        flash('Something went wrong with the database :(')
        abort(404)
    form = PostForm()
    
    if len(posts) == 0:
        flash("Database not setup or empty!")
        render_template('posts.html', posts=posts, form=form)
    
    if request.method == 'POST' and form.validate_on_submit():
        post = Post(form.title.data, form.id.data, form.author.data, form.tag_line.data, form.text.data, form.date.data)
        get_db().add_post(post)
        
    return render_template('posts.html',posts=posts,form=form)

@bp.route('/<int:post_id>/')
def get_post(post_id):
    posts = get_db().get_posts()
    for post in posts:
        if post.id == post_id:
            return render_template('post.html',post=post)
    flash("Post couldn't be found...")
    return render_template('posts.html',posts=posts,form=None)