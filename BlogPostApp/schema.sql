DROP TABLE flask_comments;
DROP TABLE flask_posts;

CREATE TABLE flask_posts(
    p_id NUMBER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    p_title VARCHAR2(100),
    p_author VARCHAR2(100),
    p_tag_line VARCHAR2(100),
    p_text CLOB,
    p_date TIMESTAMP
);

CREATE TABLE flask_comments(
    c_id NUMBER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    c_author VARCHAR2(100),
    c_text clob,
    c_post_date date,
    c_post_id NUMBER REFERENCES flask_posts(p_id) ON DELETE SET NULL
);

INSERT INTO flask_posts (p_title,p_author,p_tag_line,p_text,p_date)
VALUES ('Post 1','Will Weaver','My first post!','Wow! This is my first post!',to_date('2002-04-24', 'yyyy-mm-dd'));

INSERT INTO flask_posts (p_title,p_author,p_tag_line,p_text,p_date)
VALUES ('Post 2','Luke Weaver','Two posts???','Imagine having two posts, crazy.',to_date('2000-01-15', 'yyyy-mm-dd'));

commit;