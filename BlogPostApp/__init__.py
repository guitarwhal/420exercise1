import click
from flask import Flask, render_template
from .db import Database

@click.command('init-db')
def init_db_command():
    db = Database()
    db.run_file('./schema.sql')
    click.echo('Initialized the database.')
    db.close()

def __init__():
    app = create_app()

def create_app(test_config=None):
    from .home_views import bp as home_bp
    from .post_views import bp as post_bp
    
    app = Flask(__name__, instance_relative_config=True)
    import secrets
    app.config.from_mapping(SECRET_KEY = secrets.token_urlsafe(32))
    app.register_blueprint(home_bp)
    app.register_blueprint(post_bp)
    
    app.cli.add_command(init_db_command)
    
    if test_config is None:
        app.config.from_pyfile('config.py',silent=True)
    else:
        app.config.from_mapping(test_config)
    
    @app.errorhandler(404)
    def page_not_found(error):
        return render_template("404.html"),404

    app.register_error_handler(404,page_not_found)
    
    return app
    