class Comment:
    def _init_(self,id,author,text,post_date,post_id):
        if (not isinstance(text,str)):
            return "Comment content must be a string!"
        self.id = id
        self.author = author
        self.text = text
        self.post_date = post_date
        self.post_id = post_id
    