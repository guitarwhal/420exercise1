class Post:
    def __init__(self,id,title,author,tag_line,text,date):
        if (not isinstance(title,str)):
            return "Title must be a string!"
        self.id = id
        self.title = title
        self.author = author
        self.tag_line = tag_line
        self.text = text
        self.date = date
    
    def __repr__(self):
        return f"<h1>\"{self.title}\"</h1>\
            <h4>By: {self.author}</h4>\
                {self.text}"

    def __str__(self):
        return f"<h1><a href='../post/{self.id}/'>{self.title}</a></h1>\
            <h2>{self.tag_line}</h2>"

from flask_wtf import FlaskForm
from wtforms import StringField,IntegerField,DateField
from wtforms.validators import DataRequired,NumberRange
class PostForm(FlaskForm):
    id = IntegerField('id',
        validators = [DataRequired(), NumberRange(0)])
    title = StringField('title',
        validators = [DataRequired()])
    author = StringField('author',
        validators = [DataRequired()])
    tag_line = StringField('tag_line',
        validators = [DataRequired()])
    text = StringField('text',
        validators = [DataRequired()])
    date = DateField('date',
        validators = [DataRequired()])